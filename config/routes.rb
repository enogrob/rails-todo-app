Rails.application.routes.draw do
  devise_for :users , controllers: { registrations: 'registrations' }

  authenticated :user do
    root 'tasks#index', as: :authenticated_root
  end

  resources :tasks do
    member do
      post :toggle
    end
    resources :subtasks do
      member do
        post :toggle
      end
    end
    member do
      patch :complete
    end
  end

  root "home#index"
end
