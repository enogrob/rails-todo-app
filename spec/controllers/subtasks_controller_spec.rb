require 'rails_helper'

RSpec.describe SubtasksController, type: :controller do
  include_context "task setup"

  describe "#show" do
    it "responds with JSON formatted output" do
      sign_in user
      get :show, format: :json,
        params: { task_id: task.id, id: subtask.id }
      expect(response).to have_content_type :json
    end
  end

  describe "#create" do
    it "responds with JSON formatted output" do
      new_subtask = { name: "New test subtask" }
      sign_in user
      post :create, format: :json,
        params: { task_id: task.id, subtask: new_subtask }
      expect(response).to have_content_type :json
    end

    it "adds a new subtask to the task" do
      new_subtask = { name: "New test subtask" }
      sign_in user
      expect {
        post :create, format: :json,
          params: { task_id: task.id, subtask: new_subtask }
      }.to change(task.subtasks, :count).by(1)
    end

    it "requires authentication" do
      new_subtask = { name: "New test subtask" }
      # Don't sign in this time ...
      expect {
        post :create, format: :json,
          params: { task_id: task.id, subtask: new_subtask }
      }.to_not change(task.subtasks, :count)
      expect(response).to_not be_success
    end
  end
end
