require 'rails_helper'

RSpec.describe TasksController, type: :controller do
  describe "#index" do
    context "as an authenticated user" do
      before do
        @user = FactoryBot.create(:user)
      end

      it "responds successfully" do
        sign_in @user
        get :index
        aggregate_failures do
          expect(response).to be_success
          expect(response).to have_http_status "200"
        end
      end
    end

    context "as a guest" do
      it "returns a 302 response" do
        get :index
        expect(response).to have_http_status "302"
      end

      it "redirects to the sign-in page" do
        get :index
        expect(response).to redirect_to "/users/sign_in"
      end
    end
  end

  describe "#show" do
    context "as an authorized user" do
      before do
        @user = FactoryBot.create(:user)
        @task = FactoryBot.create(:task, owner: @user)
      end

      it "responds successfully" do
        sign_in @user
        get :show, params: { id: @task.id }
        expect(response).to be_success
      end
    end

    context "as an unauthorized user" do
      before do
        @user = FactoryBot.create(:user)
        other_user = FactoryBot.create(:user)
        @task = FactoryBot.create(:task, owner: other_user)
      end

      it "redirects to the dashboard" do
        sign_in @user
        get :show, params: { id: @task.id }
        expect(response).to redirect_to root_path
      end
    end
  end

  describe "#create" do
    context "as an authenticated user" do
      before do
        @user = FactoryBot.create(:user)
      end

      context "with valid attributes" do
        it "adds a task" do
          task_params = FactoryBot.attributes_for(:task)
          sign_in @user
          expect {
            post :create, params: { task: task_params }
          }.to change(@user.tasks, :count).by(1)
        end
      end

      context "with invalid attributes" do
        it "does not add a task" do
          task_params = FactoryBot.attributes_for(:task, :invalid)
          sign_in @user
          expect {
            post :create, params: { task: task_params }
          }.to_not change(@user.tasks, :count)
        end
      end
    end

    context "as a guest" do
      it "returns a 302 response" do
        task_params = FactoryBot.attributes_for(:task)
        post :create, params: { task: task_params }
        expect(response).to have_http_status "302"
      end

      it "redirects to the sign-in page" do
        task_params = FactoryBot.attributes_for(:task)
        post :create, params: { task: task_params }
        expect(response).to redirect_to "/users/sign_in"
      end
    end
  end

  describe "#update" do
    context "as an authorized user" do
      before do
        @user = FactoryBot.create(:user)
        @task = FactoryBot.create(:task, owner: @user)
      end

      it "updates a task" do
        task_params = FactoryBot.attributes_for(:task,
          name: "New Task Name")
        sign_in @user
        patch :update, params: { id: @task.id, task: task_params }
        expect(@task.reload.name).to eq "New Task Name"
      end
    end

    context "as an unauthorized user" do
      before do
        @user = FactoryBot.create(:user)
        other_user = FactoryBot.create(:user)
        @task = FactoryBot.create(:task,
          owner: other_user,
          name: "Same Old Name")
      end

      it "does not update the task" do
        task_params = FactoryBot.attributes_for(:task,
          name: "New Name")
        sign_in @user
        patch :update, params: { id: @task.id, task: task_params }
        expect(@task.reload.name).to eq "Same Old Name"
      end

      it "redirects to the dashboard" do
        task_params = FactoryBot.attributes_for(:task)
        sign_in @user
        patch :update, params: { id: @task.id, task: task_params }
        expect(response).to redirect_to root_path
      end
    end

    context "as a guest" do
      before do
        @task = FactoryBot.create(:task)
      end

      it "returns a 302 response" do
        task_params = FactoryBot.attributes_for(:task)
        patch :update, params: { id: @task.id, task: task_params }
        expect(response).to have_http_status "302"
      end

      it "redirects to the sign-in page" do
        task_params = FactoryBot.attributes_for(:task)
        patch :update, params: { id: @task.id, task: task_params }
        expect(response).to redirect_to "/users/sign_in"
      end
    end
  end

  describe "#destroy" do
    context "as an authorized user" do
      before do
        @user = FactoryBot.create(:user)
        @task = FactoryBot.create(:task, owner: @user)
      end

      it "deletes a task" do
        sign_in @user
        expect {
          delete :destroy, params: { id: @task.id }
        }.to change(@user.tasks, :count).by(-1)
      end
    end

    context "as an unauthorized user" do
      before do
        @user = FactoryBot.create(:user)
        other_user = FactoryBot.create(:user)
        @task = FactoryBot.create(:task, owner: other_user)
      end

      it "does not delete the task" do
        sign_in @user
        expect {
          delete :destroy, params: { id: @task.id }
        }.to_not change(Task, :count)
      end

      it "redirects to the dashboard" do
        sign_in @user
        delete :destroy, params: { id: @task.id }
        expect(response).to redirect_to root_path
      end
    end

    context "as a guest" do
      before do
        @task = FactoryBot.create(:task)
      end

      it "returns a 302 response" do
        delete :destroy, params: { id: @task.id }
        expect(response).to have_http_status "302"
      end

      it "redirects to the sign-in page" do
        delete :destroy, params: { id: @task.id }
        expect(response).to redirect_to "/users/sign_in"
      end

      it "does not delete the task" do
        expect {
          delete :destroy, params: { id: @task.id }
        }.to_not change(Task, :count)
      end
    end
  end

  describe "#complete" do
    context "as an authenticated user" do
      let!(:task) { FactoryBot.create(:task, completed: nil) }

      before do
        sign_in task.owner
      end

      describe "an unsuccessful completion" do
        before do
          allow_any_instance_of(Task).
            to receive(:update_attributes).
            with(completed: true).
            and_return(false)
        end

        it "redirects to the task page" do
          patch :complete, params: { id: task.id }
          expect(response).to redirect_to task_path(task)
        end

        it "sets the flash" do
          patch :complete, params: { id: task.id }
          expect(flash[:alert]).to eq "Unable to complete task."
        end

        it "doesn't mark the task as completed" do
          expect {
            patch :complete, params: { id: task.id }
          }.to_not change(task, :completed)
        end
      end
    end
  end
end
