require 'rails_helper'

RSpec.feature "Tasks Features", type: :feature do
  scenario "user creates a new task" do
    user = FactoryBot.create(:user)
    # using our customer login helper:
    # sign_in_as user
    # or the one provided by Devise:
    login_as user, scope: :user
    visit root_path

    expect {
      click_link "Add Task"
      fill_in "Name", with: "Test Task"
      click_button "Create Task"

      aggregate_failures do
        expect(page).to have_content "Task was successfully created"
        expect(page).to have_content "Test Task"
        expect(page).to have_content "Owner: #{user.email}"
      end
    }.to change(user.tasks, :count).by(1)
  end

  scenario "user completes a task by editing" do
    user = FactoryBot.create(:user)
    task = FactoryBot.create(:task, name: "Test task", owner: user)
    login_as user, scope: :user

    visit edit_task_path(task)
    expect(page).to have_content "Completed"

    check('Completed')

    click_button "Update Task"

    expect(task.reload.completed?).to be true
    expect(page).to \
      have_content "Task was successfully updated"
    expect(page).to have_content "Test task"
  end

  scenario "user setup a public task by editing" do
    user = FactoryBot.create(:user)
    task = FactoryBot.create(:task, name: "Test task", owner: user)
    login_as user, scope: :user

    visit edit_task_path(task)
    expect(page).to have_content "Public"

    check('Public')

    click_button "Update Task"

    expect(task.reload.public?).to be true
    expect(page).to \
      have_content "Task was successfully updated"
    expect(page).to have_content "Test task"
  end

  scenario "user toggles a task", js: true do
    user = FactoryBot.create(:user)
    task = FactoryBot.create(:task, name: "Task test", owner: user)
    login_as user, scope: :user
    visit task_path(task)
    check "Task test"
    aggregate_failures do
      expect(page).to have_css "label.completed", text: "Task test"
      expect(task.reload).to be_completed
    end
  end
end
