require 'rails_helper'

RSpec.feature "Subtasks Features", type: :feature do
  let(:user) { FactoryBot.create(:user) }
  let(:task) {
    FactoryBot.create(:task,
                       name: "Task test",
                       owner: user)
  }
  let!(:subtask) { task.subtasks.create!(name: "Task test subtask") }

  scenario "user creates a new subtask" do
    login_as user, scope: :user
    visit task_path(task)

    expect {
      click_link "Add Subtask"
      fill_in "Name", with: "Test Subtask"
      click_button "Create Subtask"

      aggregate_failures do
        expect(page).to have_content "Subtask was successfully created"
        expect(page).to have_content "Test Subtask"
      end
    }.to change(task.subtasks, :count).by(1)
  end

  scenario "user completes a subtask by editing" do
    login_as user, scope: :user

    visit edit_task_subtask_path(task, subtask)
    expect(page).to have_content "Completed"

    check('Completed')

    click_button "Update Subtask"

    expect(subtask.reload.completed?).to be true
    expect(page).to \
      have_content "Subtask was successfully updated"
    expect(page).to have_content "Task test subtask"
  end

  scenario "user toggles a subtask", js: true do
    login_as user, scope: :user
    visit task_path(task)
    complete_subtask "Task test subtask"
    expect_complete_subtask "Task test subtask"

    undo_complete_subtask "Task test subtask"
    expect_incomplete_subtask "Task test subtask"
  end

  def complete_subtask(name)
    check name
  end

  def undo_complete_subtask(name)
    uncheck name
  end

  def expect_complete_subtask(name)
    aggregate_failures do
      expect(page).to have_css "label.completed", text: name
      expect(subtask.reload).to be_completed
    end
  end

  def expect_incomplete_subtask(name)
    aggregate_failures do
      expect(page).to_not have_css "label.completed", text: name
      expect(subtask.reload).to_not be_completed
    end
  end
end
