FactoryBot.define do
  factory :task do
    sequence(:name) { |n| "Task #{n}" }
    association :owner

    trait :with_subtasks do
      after(:create) { |task| create_list(:subtask, 5, task: task) }
    end

    trait :invalid do
      name nil
    end

  end
end
