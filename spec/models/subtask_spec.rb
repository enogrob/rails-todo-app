require 'rails_helper'

RSpec.describe Subtask, type: :model do
  let(:task) { FactoryBot.create(:task) }

  it "is valid with a task and name" do
    subtask = Subtask.new(
      task: task,
      name: "Test subtask",
      completed: false
    )
    expect(subtask).to be_valid
  end

  it "is invalid without a task" do
    subtask = Subtask.new(
        task: nil,
        name: "Test subtask",
        completed: false
    )
    expect(subtask).not_to be_valid
  end

  it "is invalid without a name" do
    task = Task.new(name: nil)
    task.valid?
    expect(task.errors[:name]).to include("can't be blank")
  end
end
