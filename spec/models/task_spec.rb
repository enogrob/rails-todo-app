require 'rails_helper'

RSpec.describe Task, type: :model do
  let(:user) { FactoryBot.create(:user) }

  it { is_expected.to validate_uniqueness_of(:name).scoped_to(:user_id) }

  it "is valid with a name and user" do
    task = Task.new(
        user_id: user[:id],
        name: "Test task",
        completed: false,
        public: false
    )
    expect(task).to be_valid
  end

  it "is invalid without a user" do
    task = Task.new(
        name: "Test task",
        completed: false,
        public: false
    )
    expect(task).not_to be_valid
  end

  it "is invalid without a name" do
    task = Task.new(name: nil)
    task.valid?
    expect(task.errors[:name]).to include("can't be blank")
  end

  it "can have many subtasks" do
    task = FactoryBot.create(:task, :with_subtasks)
    expect(task.subtasks.length).to eq 5
  end
end
