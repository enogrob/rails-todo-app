RSpec.shared_context "task setup" do
  let(:user) {FactoryBot.create(:user) }
  let(:task) { FactoryBot.create(:task, owner: user) }
  let(:subtask) { task.subtasks.create!(name: "Test task") }
end
