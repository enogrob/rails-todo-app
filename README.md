## rails-todo-app

![thebrain](app/assets/images/thebrain.png)

```text
(C) 2018 Crafted by Roberto Nogueira
email : enogrob@gmail.com
Mobile: +55 (12) 98124-5001
```

### Download, about the Application and Database

It is a `Rails` application structured in 6 branches and the following commits:

```shell
$ git clone git@bitbucket.org:enogrob/rails-todo-app.git
$ cd rails-todo-app
$ git branch
  01-initial
  02-rspec
  03-devise
  04-models-bootstrap
  05-docker
  06-docs
* master

$ git log --oneline
5586eac performed-README.md
7529d09 corrected-sign-up-for-registrations
bfe2c10 add-docker-support
35fc5f3 performed-controllers-features-test
83aca45 performed-models-test
ed8ccd4 add-public-tasks-in-index
445aa8d defined-controllers-views
a596e62 add-models-bootstrap
ea82d33 add-authentication-devise-support
50e0985 add-rspec-test-support
6afdfed initial-commit
```

As it is a simple test application, its was used `sqlite` so the databases creation and migration is required.
```shell
$ rails db:create:all
$ rails db:migrate
```
**Model Dependency Diagram**

![model diagram](app/assets/images/screenshot_9693.png)


### Running the Application

This README would help  whatever steps are necessary to get the application and tests up and running.

**Ruby and Rails version**

Checking the `Ruby` and `Rails` version.
```shell
$ rvm current
ruby-2.5.0@rails-5.1.5
```

**Starting the Rails server**

Starting `Rails` server with development environment and access with the browser `http://localhost:3000`.
```shell
$ cd rails-todo-app
$ rails server
```

or

Starting `Rails` server with its container as daemon and access via the browser the same way.
So build the images first if it was not done before. Also it is done as an autobuild project in `Dockerhub`.
```shell
$ docker-compose build
$ docker-compose up
```
**Screens shots**

![splash](app/assets/images/screenshot_9694.png)

![sign-up](app/assets/images/screenshot_9695.png)

![sign-in](app/assets/images/screenshot_9696.png)



**Stopping the Rails Server**

If you have started within the development environment just press `ctrl-c`.

If it was with `Docker` run the following command:
```shell
docker-compose down
```

#### Testing with RSpec

Within `Test Environment`.
```shell
$ cd rails-todo-app
$ rspec

HomeController
  #index
    responds succesfully
    returns a 200 response

SubtasksController
  #show
    responds with JSON formatted output
  #create
    responds with JSON formatted output
    adds a new subtask to the task
    requires authentication

TasksController
  #index
    as an authenticated user
      responds successfully
    as a guest
      returns a 302 response
      redirects to the sign-in page
  #show
    as an authorized user
      responds successfully
    as an unauthorized user
      redirects to the dashboard
  #create
    as an authenticated user
      with valid attributes
        adds a task
      with invalid attributes
        does not add a task
    as a guest
      returns a 302 response
      redirects to the sign-in page
  #update
    as an authorized user
      updates a task
    as an unauthorized user
      does not update the task
      redirects to the dashboard
    as a guest
      returns a 302 response
      redirects to the sign-in page
  #destroy
    as an authorized user
      deletes a task
    as an unauthorized user
      does not delete the task
      redirects to the dashboard
    as a guest
      returns a 302 response
      redirects to the sign-in page
      does not delete the task
  #complete
    as an authenticated user
      an unsuccessful completion
        redirects to the task page
        sets the flash
        does not mark the task as completed

Subtasks Features
  user creates a new subtask
  user completes a subtask by editing
  user toggles a subtask

Tasks Features
  user creates a new task
  user completes a task by editing
  user setup a public task by editing
  user toggles a task

Subtask
  is valid with a task and name
  is invalid without a task
  is invalid without a name

Task
  should validate that :name is case-sensitively unique within the scope of :user_id
  is valid with a name and user
  is invalid without a user
  is invalid without a name
  can have many subtasks

User
  has a valid factory
  is valid email, and password
  should validate that :email cannot be empty/falsy
  should validate that :email is case-insensitively unique

Finished in 10.21 seconds (files took 6.46 seconds to load)
48 examples, 0 failures
$
```

Within `Docker`.
The test results are likely the same as running locally.
Starting containers as Daemon.
```shell
$ docker-compose up -d

$ docker-compose ps
Name                   Command               State           Ports
--------------------------------------------------------------------------------
rails-todo-app   bundle exec rails s -p 300 ...   Up      0.0.0.0:3000->3000/tcp
selenium         /opt/bin/entry_point.sh          Up      4444/tcp

$ docker-compose run web rspec
Starting selenium ... done
:
```

**References**

Gems
1. [rspec-rails](https://rubygems.org/gems/rspec-rails) - is a testing framework for Rails.
2. [devise](https://rubygems.org/gems/devise) - is a flexible authentication solution for Rails with Warden.
3. [bootstrap-sass](https://rubygems.org/gems/bootstrap-sass) -  is a Sass-powered version of Bootstrap 3, ready to drop right into your Sass powered applications.
4. [capybara](https://rubygems.org/gems/capybara) - is an integration testing tool for rack based web applications.
