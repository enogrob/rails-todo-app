class SubtasksController < ApplicationController
  before_action :set_task
  before_action :task_owner?
  before_action :set_subtask, only: [:show, :edit, :update, :destroy, :toggle]

  # GET /subtasks
  # GET /subtasks.json
  def index
    @subtasks = Subtask.all
  end

  # GET /subtasks/1
  # GET /subtasks/1.json
  def show
  end

  # GET /subtasks/new
  def new
    @subtask = @task.subtasks.new
  end

  # GET /subtasks/1/edit
  def edit
  end

  # POST /subtasks
  # POST /subtasks.json
  def create
    @subtask = @task.subtasks.new(subtask_params)

    respond_to do |format|
      if @subtask.save
        format.html { redirect_to @task, notice: 'Subtask was successfully created.' }
        format.json { render :show, status: :created, location: [@subtask.task, @subtask] }
      else
        format.html { render :new }
        format.json { render json: @subtask.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /subtasks/1
  # PATCH/PUT /subtasks/1.json
  def update
    respond_to do |format|
      if @subtask.update(subtask_params)
        format.html { redirect_to @task, notice: 'Subtask was successfully updated.' }
        format.json { render :show, status: :ok, location: @subtask }
      else
        format.html { render :edit }
        format.json { render json: @subtask.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subtasks/1
  # DELETE /subtasks/1.json
  def destroy
    @subtask.destroy
    respond_to do |format|
      format.html { redirect_to @task, notice: 'Subtask was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def toggle
    @subtask.update_attributes(completed: !@subtask.completed)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subtask
      @subtask = Subtask.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subtask_params
      params.require(:subtask).permit(:name, :completed, :task_id)
    end
end
