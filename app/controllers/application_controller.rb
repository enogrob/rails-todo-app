class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :authenticate_user!

  protected

  def set_task
    @task = Task.find(params[:task_id])
  end

  def set_public_tasks
    @public_tasks = Task.where(public: true)
  end

  def task_owner?
    unless @task.owner == current_user
      redirect_to root_path, alert: "You don't have access to that task."
    end
  end
end
