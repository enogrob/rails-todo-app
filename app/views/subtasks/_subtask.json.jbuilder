json.extract! subtask, :id, :name, :completed, :task_id, :created_at, :updated_at
json.url task_subtask_url(subtask.task, format: :json)
